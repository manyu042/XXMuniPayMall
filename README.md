# uni-pay支付商城项目开发，随堂笔记

![](https://gitee.com/qingnian8/XXMuniPayMall/raw/master/%E9%85%8D%E5%A5%97%E5%AD%A6%E4%B9%A0%E8%B5%84%E6%96%99/uniPayMall.jpg)

### **🚀学习地址：[完整版视频教程☚☚☚](https://study.163.com/course/courseMain.htm?courseId=1213501804&share=2&shareId=480000002305454)**

####  微信扫码小程序，体验咸虾米商城

![](https://mp-3309c116-4743-47d6-9979-462d2edf878c.cdn.bspapp.com/xxmPath/xxm-mp-code.jpg)



> 纯前端实现H5、小程序、APP商城开发；
>
> 技术栈：uniapp+uniCloud+uni-id+unipay+uni-ui+uView+vuex； 
>
> 学完可开发定制化商城，web前端技术再进阶一大步；
>
> 开源不易，给个⭐️star吧
>
> 学习交流Q群：497320816

![](https://gitee.com/qingnian8/XXMuniPayMall/raw/master/%E9%85%8D%E5%A5%97%E5%AD%A6%E4%B9%A0%E8%B5%84%E6%96%99/xxmQq.png)

------



# 📜第1章 开发项目前的准备

### 1.官方文档

[uniapp官方文档](https://uniapp.dcloud.net.cn/)

[uniPay统一支付](https://uniapp.dcloud.net.cn/uniCloud/uni-pay.html)

### 2.学习前的知识储备

[uniapp零基础入门篇](https://www.bilibili.com/video/BV1mT411K7nW)

[uniCloud云开发入门篇](https://www.bilibili.com/video/BV1PP411E7qG)

### 3.uniPay支付需要个体工商或企业营业执照

> 个人是无法开通微信商户和支付宝商户的，最低需要个体工商户的营业执照才能申请；
>
> 难道个人就无法使用uniPay的在线支付了吗？
>
> 个人是可以去淘宝买个代办营业执照，非常便宜，不用了也可以随时注销。

🔥[淘宝办理个体营业执照链接](https://s.click.taobao.com/ihiWbGu)

这个是up主对比全网最便宜的一款了，可以办理海南的才50块钱全国通用，该商铺只是up主推荐（非UP办理责任不负责），你可以自己去其他店铺选择，反正up用着是可行的，应届生或者上班族建议不要用自己的身份证去申请，虽说没什么问题，还是建议用自己家人的身份证去申请。

------



# 📜第2章 初始化项目及首页页面布局

### 1.常用的UI组件库

[第三方组件库uViews](https://www.uviewui.com/)

[uniapp扩展组件uni-ui](https://uniapp.dcloud.net.cn/component/uniui/uni-ui.html)

### 2.mixin混合指令

**多行文本出现省略号**

```
@mixin ellipsis($row:1){
	text-overflow: -o-ellipsis-lastline;
	overflow: hidden;				//溢出内容隐藏
	text-overflow: ellipsis;		//文本溢出部分用省略号表示
	display: -webkit-box;			//特别显示模式
	-webkit-line-clamp: $row;			//行数
	line-clamp: $row;					
	-webkit-box-orient: vertical;	//盒子中内容竖直排列	
}
```



### 3.获取系统信息的同步接口

[getSystemInfoSync系统信息接口](https://uniapp.dcloud.net.cn/api/system/info.html#getsysteminfosync)

------



# 📜第3章 Vuex状态管理相关

### 1.vuex介绍

[vuex官方文档](https://vuex.vuejs.org/zh/)

> Vuex 是一个专为 Vue.js 应用程序开发的**状态管理模式 + 库**。它采用集中式存储管理应用的所有组件的状态，并以相应的规则保证状态以一种可预测的方式发生变化。

### 2.Module

> 由于使用单一状态树，应用的所有状态会集中到一个比较大的对象。当应用变得非常复杂时，store 对象就有可能变得相当臃肿。
>
> 为了解决以上问题，Vuex 允许我们将 store 分割成**模块（module）**。每个模块拥有自己的 state、mutation、action、getter



# 📜第4章 **商城订单页面构建**

### 1.css的filter滤镜

filters是CSS3里新增的一种神奇的功能，一般我们提及滤镜，就会想到使用PhotoShop制作的图片，但是使用CSS滤镜不需要任何作图软件，仅使用CSS就会生成多种的滤镜效果，例如模糊效果、透明效果、色彩反差调整等等；同时，CSS滤镜不仅可以对图片进行滤镜处理，还可以对网页元素甚至视频进行滤镜处理。
**filter: grayscale(100%); **

[拓展知识地址](https://blog.csdn.net/zg0601/article/details/120917933)

### 2.配送方式用Cursor AI写代码

[学习地址点这里](https://www.bilibili.com/video/BV1To4y1K78d)



### 3.uniapp中完整的条件编译

[跳转传送门](https://uniapp.dcloud.net.cn/tutorial/platform.html)



# 📜第5章 **后台管理端相关页面**

### 1.小程序 subPackages分包

因小程序有体积和资源加载限制，各家小程序平台提供了分包方式，优化小程序的下载和启动速度。

[跳转传送门](https://uniapp.dcloud.net.cn/collocation/pages.html#subpackages)

------



# 🚀后续学习地址：[完整版视频教程☚☚☚](https://study.163.com/course/courseMain.htm?courseId=1213501804&share=2&shareId=480000002305454)



------

![](/配套学习资料/allkt1.jpg)

![](/配套学习资料/allkt2.jpg)